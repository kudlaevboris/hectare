'use strict';

// npm init
// npm i gulp fs -D
// gulp install:path
// gulp install:packages
// копировать строку и установить остальные пакеты



let gulp = require('gulp'),
	fileSystem = require('fs'); // проверка на существование файлов и папок
/*
var babel = require('gulp-babel')
gulp.task('babel', () => {
  return gulp.src('src/app.js')
  .pipe(babel({presets: ['minify']}))
  .pipe(gulp.dest('dist'))
})
*/
let babel = require('gulp-babel');
let psi = require('psi');
let site = 'http://localhost:/';

let srcPath = {
	dist: {
		less: 'dist/assets/css/',
		sass: 'dist/assets/sass/',
		js: 'dist/assets/js/',
		img: 'dist/assets/images/',
		svg: 'dist/assets/images/svg/',
		html: 'dist/**/',
	},
	public: {
		libs: 'public/assets/library',
		css: 'public/assets/css',
		js: 'public/assets/js',
		img: 'public/assets/images',
		svg: 'public/assets/images/svg',
		html: 'public/'
	},
	nodeModules: './node_modules/'
};

let serverCfg = {
	baseDirectory: 'public/',
	host: 'localhost',
	port: 3000
};

// создание структуры папок
gulp.task('install:path', function() {
	if (!fileSystem.existsSync('./dist')) {
		fileSystem.mkdirSync('dist');
		fileSystem.mkdirSync('dist/json');
		fileSystem.mkdirSync('dist/assets');
		fileSystem.mkdirSync('dist/assets/css');
		fileSystem.mkdirSync('dist/assets/sass');
		fileSystem.mkdirSync('dist/assets/js');
		fileSystem.mkdirSync('dist/assets/images');
		fileSystem.mkdirSync('dist/assets/images/svg');
		fileSystem.writeFile('dist/index.html', '<!DOCTYPE html>', 'utf-8');
		fileSystem.writeFile('./.gitignore', 'node_modules/\npublic/\n', 'utf-8');
	}
	if (!fileSystem.existsSync('./public')) {
		fileSystem.mkdirSync('./public/');
	}
});

// поиск нужного пакета (вывод строки установки остальных пакетов)
gulp.task('install:packages', function() {
	let text = '',
		packages = [
			'gulp-file-include',
			'gulp-less',
			'gulp-sass',
			'gulp-minify-css',
			'gulp-sourcemaps',
			'gulp-strip-comments',
			'gulp-uglify',
			'gulp-imagemin',
			'gulp-newer',
			'gulp-svgo',
			'browser-sync',
			//'gulp-babel'
		];

	for (let i = 0; i < packages.length; i++) {
		if (!checkLibriaryFolder(packages[i])) {
			text += packages[i] + ' ';
		}
	}

	if (text === '') {
		console.info('All packages success');
	} else {
		console.info('Install packages');
		console.info('');
	}
	console.info('npm i ' + text + '-D');
});

// добавление библиотеки jquery
gulp.task('l:jquery', function() {
	if (checkLibriaryFolder('jquery')) {
		console.log('Jquery install');
		gulp.src(srcPath.nodeModules + 'jquery/dist/*.js')
			.pipe(gulp.dest(srcPath.public.libs + '/jquery'));
	} else {
		console.info('npm i jquery -D');
	}
});

// добавление библиотеки jquery JSTree
// https://www.jstree.com/
gulp.task('l:jstree', function() {
	if (checkLibriaryFolder('jstree')) {
		console.log('Jstree install');
		gulp.src(srcPath.nodeModules + 'jstree/dist/**/*')
			.pipe(gulp.dest(srcPath.public.libs + '/jquery/jstree/'));
	} else {
		console.info('npm i jstree -D');
	}
});

// добавление библиотеки jquery Cookie
// https://www.npmjs.com/package/jquery.cookie
gulp.task('l:jquery.cookie', function() {
	if (checkLibriaryFolder('jquery.cookie')) {
		console.log('Jquery cookie install');
		gulp.src(srcPath.nodeModules + 'jquery.cookie/jquery.cookie.js')
			.pipe(gulp.dest(srcPath.public.libs + '/jquery/cookie/'));
	} else {
		console.info('npm i jquery.cookie -D');
	}
});

// добавление библиотеки bootstrap
// http://getbootstrap.com/
gulp.task('l:bootstrap', function() {
	if (checkLibriaryFolder('bootstrap')) {
		console.log('Bootstrap install');
		gulp.src([
				'./node_modules/bootstrap/dist/**/*.js',
				'./node_modules/bootstrap/dist/**/*.css'
			])
			.pipe(gulp.dest(srcPath.public.libs + '/bootstrap'));
		gulp.src(['./node_modules/bootstrap/dist/fonts/*'])
			.pipe(gulp.dest(srcPath.public.libs + '/bootstrap/fonts'));
	} else {
		console.info('npm i bootstrap -D');
	}
});

// проверка на существование папки
function checkLibriaryFolder(folderName) {
	if (fileSystem.existsSync(srcPath.nodeModules + folderName)) {
		return true;
	}
	console.error('Module "'+ folderName +'" no exist.');
	return false;
}

// преобразование html
gulp.task('html', function() {
	var fileInclude = require('gulp-file-include'); // подключение файлов
	gulp.src([srcPath.dist.html + '*.html', '!' + srcPath.dist.html + '_*.html'])
		.pipe(fileInclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest(srcPath.public.html));
});

gulp.task('style', ['less'], function() {
	gulp.start('min:css');
	console.log('Finish style');
});
// преобразование less в css
gulp.task('less', function() {
 let less = require('gulp-less'); // для преобразования less -> css
 return gulp.src([srcPath.dist.less + '*.less', '!' + srcPath.dist.less + '_*.less'])
  .pipe(less(
  /* {
    paths: [ path.join(__dirname, 'less', 'includes')]
   }*/
  )).pipe(gulp.dest(srcPath.public.css));

});

// преобразование sass в css
gulp.task('sass', function() {
	let less = require('gulp-sass'); // для преобразования saas -> css
	gulp.src([srcPath.dist.sass + '*.sass', '!' + srcPath.dist.sass + '_*.sass'])
		.pipe(less(
		/*	{
				paths: [ path.join(__dirname, 'less', 'includes')]
			}*/
		)).pipe(gulp.dest(srcPath.public.css));
});

// минимизации css
gulp.task('min:css', function() {
if (fileSystem.existsSync(srcPath.public.css + '/style.css')) {
    console.log('Found file');
} else {
   console.log('File not Found');

}
let minifyCss = require('gulp-minify-css'),
  rename = require('gulp-rename'); // переименование файлов
 gulp.src([srcPath.public.css + '/*.css', '!' + srcPath.public.css + '/*.min.css'])
  .pipe(minifyCss({
   keepBreaks: true
  }))
  .pipe(rename({
   suffix: '.min'
  }))
  .pipe(gulp.dest(srcPath.public.css));
});

// копирование оригинального и минимизация js
gulp.task('min:js', ['copy:js', /*'minify:js',*/ 'babelmin:js'], function() {

});

// копирование оригинального js файла без комментариев
gulp.task('copy:js', function () {
	let strip = require('gulp-strip-comments'); // удаление комментариев
	gulp.src(srcPath.dist.js + '*.js')
		.pipe(strip())
		.pipe(gulp.dest(srcPath.public.js));
});

/*
// минимизация js
gulp.task('minify:js', function () {
	var uglify = require('gulp-uglify'), // javascript компрессор
		rename = require('gulp-rename'); // переименование файлов
	gulp.src([srcPath.dist.js + '*.js', '!' + srcPath.dist.js + '*.min.js'])
		.pipe(uglify())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(srcPath.public.js));
});
*/


// минификация es2015 через babel
gulp.task('babelmin:js', function() {
	let rename = require("gulp-rename");
	let uglify = require('gulp-uglify-es').default;
	gulp.src([srcPath.dist.js + '*.js', '!' + srcPath.dist.js + '*.min.js'])
		.pipe(babel({presets: ['env']}))
		.pipe(uglify(/* options */))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(srcPath.public.js));
});

// оптимизация картинок
gulp.task('image', function () {
	let imagemin = require('gulp-imagemin'),
		newer = require('gulp-newer');
	gulp.src([srcPath.dist.img + '*.jpg', srcPath.dist.img + '*.jpeg', srcPath.dist.img + '*.png'])
		.pipe(newer(srcPath.public.img))
		.pipe(imagemin({
			progressive: true
		}))
		.pipe(gulp.dest(srcPath.public.img));
});

// оптимизация svg
gulp.task('svg', function() {
let svgo = require('gulp-svgo'); // для работы с svg
gulp.src(srcPath.dist.svg + '*.svg')
  .pipe(svgo({
        plugins: [
          {cleanupIDs: false}
        ]
  }))
  .pipe(gulp.dest(srcPath.public.svg))
});

// запуск сервера
gulp.task('browser-sync', function() {
	let browserSync = require('browser-sync'); // синхронизация с браузером
	browserSync({
		server: {
			baseDir: serverCfg.baseDirectory
		},
		host: serverCfg.host,
		port: serverCfg.port
	});
});

// перезапуск сервера
gulp.task('bs-reload', function () {
	let browserSync = require('browser-sync'); // синхронизация с браузером
	browserSync.reload();
});

// слежение за изменениями
gulp.task('watch', function () {
	gulp.watch(srcPath.dist.js + '*.js', ['min:js']);
	gulp.watch(srcPath.dist.less + '*.less', ['less']);
	gulp.watch(srcPath.public.css + '/*.css', ['min:css']);
	gulp.watch(srcPath.dist.html + '*.html', ['html']);
	gulp.watch(srcPath.dist.img + '*', ['image']);
	gulp.watch(srcPath.dist.svg + '*.svg', ['svg']);
	gulp.watch(srcPath.public.html + '**', ['bs-reload']);
	gulp.watch(srcPath.public.css + '**', ['bs-reload']);
	gulp.watch(srcPath.public.js + '**', ['bs-reload']);
	gulp.watch(srcPath.public.img + '**', ['bs-reload']);
});

// собрать проект проекта
gulp.task('build', ['html', 'less', 'min:js', 'image', 'svg'], function() {
	gulp.start('min:css');
	console.log('Finish build');
});

// для разработчика
gulp.task('dev', ['build', 'watch', 'browser-sync'], function() {
	gulp.start('min:css');
	console.log('Start develover');
});

// задачи по умолчанию
gulp.task('default', [
//	'libs',
	'dev'
]);


gulp.task('speed:mobile', function() {
	return psi(site, {
	//	key: key
		nokey: 'true',
		strategy: 'mobile',
	}).then(function(data) {
		console.log('SPEED MOBILE');
		console.log('URL:                ' + data.id);
		console.log('Total Request :     ' + convertBytes(data.pageStats.totalRequestBytes));
		console.log('--------------------------------');
		console.log('URL title:          ' + data.title);
		console.log('Speed score:        ' + data.ruleGroups.SPEED.score);
		console.log('Usability score:    ' + data.ruleGroups.USABILITY.score);
		console.log('--------------------------------');
		console.log('HTML size:          ' + convertBytes(data.pageStats.htmlResponseBytes));
		console.log('CSS size:           ' + convertBytes(data.pageStats.cssResponseBytes));
		console.log('JS size:            ' + convertBytes(data.pageStats.javascriptResponseBytes));
		console.log('Images size:        ' + convertBytes(data.pageStats.imageResponseBytes));
		console.log('Other size:         ' + convertBytes(data.pageStats.otherResponseBytes));
		console.log('Hosts:              ' + data.pageStats.numberHosts);
		console.log('Resources Total:    ' + data.pageStats.numberResources);
		console.log('Resources (static): ' + data.pageStats.numberStaticResources);
		console.log('Resources JS:       ' + data.pageStats.numberJsResources);
		console.log('Resources CSS:      ' + data.pageStats.numberCssResources);
	});
});

gulp.task('speed:desktop', function() {
	return psi(site, {
		nokey: 'true',
	//	key: key,
		strategy: 'desktop',
	}).then(function (data) {
		console.log('SPEED DESKTOP');
		console.log('URL:                ' + data.id);
		console.log('Total Request :     ' + convertBytes(data.pageStats.totalRequestBytes));
		console.log('--------------------------------');
		console.log('URL title:          ' + data.title);
		console.log('Speed score:        ' + data.ruleGroups.SPEED.score);
		console.log('--------------------------------');
		console.log('HTML size:          ' + convertBytes(data.pageStats.htmlResponseBytes));
		console.log('CSS size:           ' + convertBytes(data.pageStats.cssResponseBytes));
		console.log('JS size:            ' + convertBytes(data.pageStats.javascriptResponseBytes));
		console.log('Images size:        ' + convertBytes(data.pageStats.imageResponseBytes));
		console.log('Other size:         ' + convertBytes(data.pageStats.otherResponseBytes));
		console.log('Hosts:              ' + data.pageStats.numberHosts);
		console.log('Resources Total:    ' + data.pageStats.numberResources);
		console.log('Resources (static): ' + data.pageStats.numberStaticResources);
		console.log('Resources JS:       ' + data.pageStats.numberJsResources);
		console.log('Resources CSS:      ' + data.pageStats.numberCssResources);
	});
});

function convertBytes(bytes) {
	bytes = parseInt(bytes);
	let DEF = 1024;
	if (bytes < DEF) {
		return bytes + ' B';
	} else if (bytes < DEF * DEF) {
		return (bytes / DEF).toFixed(2) + ' kB';
	} else if (bytes < DEF * DEF * DEF) {
		return (bytes / DEF * DEF).toFixed(2) + ' MB';
	} else if (bytes < DEF * DEF * DEF * DEF) {
		return (bytes / DEF * DEF * DEF).toFixed(2) + ' GB';
	}
	return '';
}