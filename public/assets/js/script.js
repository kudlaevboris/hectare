"use strict";


$(document).ready(function(){

	$(document).on('click', "a",  function() {

		var toLoad = $(this).attr('href')+' #page-wrapper';
		var toSRC = $(this).attr('href');
		$('#page-wrapper').fadeIn('fast',loadContent());
		history.pushState(null, null, toSRC);
		function loadContent() {
			$('#page-wrapper').load(toLoad,'',showNewContent);
			$('.menu_wrapper').removeClass('active');
			$('#page-wrapper').removeClass('background-none');
			$('.preload-wrapper').addClass('animate');

					}
		function showNewContent() {
			$(this).children().unwrap();
			setTimeout(function(){
				$('.preload-wrapper').removeClass('animate');
			},1000)
			$('.content').hide();
			if ($('.menu_wrapper').is('.active')) {
				$('.menu_wrapper').removeClass('active');
				setTimeout(function(){
					$('.content').fadeOut('800');
				},800)
				setTimeout(function(){
					$('.content').fadeIn('800');
				},1600);
			} else {
				$('.content').fadeOut('800');
				setTimeout(function(){
					$('.content').fadeIn('800');
				},1200);
			}
			if ($('section').is('.sect__what-for')){
				sect_what_for();
			}
			if  ($('section').is('.sect__what')) {
				sect_what();
			}
			if ($('section').is('.sect__where')) {
				sect_where();
			}
			if ($('section').is('.sect__feedback')) {
				sect_feedback();
			}
			if ($('section').is('.sect__support')){
				sect_support();
			}
			if ($('section').is('.sect__history')) {
				sect_history();
			}
			if ($('section').is('.sect__how-many-feedback')) {
				sect_how_many_feedback();
			}
			if ($('section').is('.sect__how-many')) {
				sect_how_many();
			}
		}
		return false;
	});




		$('.menu__btn').on('click', function(){
		$('.menu_wrapper').addClass('active');
	});

	$('.menu__close').on('click', function(){
		$('.menu_wrapper').removeClass('active');
	});

	sect_where()
	function sect_where() {
		$('body').on('click', function(event){
			if (!$(event.target).parents('g').is('.region-svg') && !$(event.target).parents('').is('.region__info')) {
				$('.region__info').removeClass('active');
				$('.region-svg').removeClass('active');
				$('#map').removeClass('active');
			}
		});
		if ($('section').is('.sect__where')) {
			$('#page-wrapper').addClass('background-none');
			$('.content').fadeIn('400');
		}
		$('.map g.region-svg').on('click', function(event){
			if ($('.region-svg').is('.active')) {
				$('.map g.region-svg').removeClass('active');
				$('.region__info').removeClass('active');
				$(this).addClass('active');
				$('#map').addClass('active');
				setTimeout(function(){
					region_active(event);
				}, 600)
			} else {
				$('#map').addClass('active');
				$('.map g.region-svg').removeClass('active');
				$(this).addClass('active');
				region_active(event);
			}
		});
		$('.region__close').on('click', function(){
			$('.region__info').removeClass('active');
			$('.region-svg').removeClass('active');
			$('#map').removeClass('active');
		});

		$('.region__info .main-btn').on('click', function(){
			$('.region__info').removeClass('active');
		});

		$('.region_history a').on('click', function(){
			$('.region__info').removeClass('active');
		});

		setTimeout(function(){
			$('.map_btn-activation').fadeOut();
			$('.sect__where').removeClass('map-disabled');
		},3000);


		$('.map_btn-activation').on('click', function(){
			$(this).fadeOut(400);
			setTimeout(function(){
				$('.sect__where').removeClass('map-disabled');
			},600)
		});

		contentScroll();

		let locat = location.href;
		let arr_location = locat.split('#');
		setTimeout(function(){
			$('.region__info.region_'+arr_location[1]+'').addClass("active");
		},2000)
	}

	function region_active(event) {
		content_scrollTop();
		if ($(event.target).parents('g').is('#region-map-bili')) {
			$('.region__info.region_bili').addClass('active');
		}
		if ($(event.target).parents('g').is('#region-map-egve')) {
			$('.region__info.region_egve').addClass('active');
		}
		if ($(event.target).parents('g').is('#region-map-pevek')) {
			$('.region__info.region_pevek').addClass('active');
		}
		if ($(event.target).parents('g').is('#region-map-anad')) {
			$('.region__info.region_anad').addClass('active');
		}
		if ($(event.target).parents('g').is('#region-map-chu')) {
			$('.region__info.region_chu').addClass('active');
		}
		if ($(event.target).parents('g').is('#region-map-prov')) {
			$('.region__info.region_prov').addClass('active');
		}
	}


	sect_what_for();
	function sect_what_for() {
		$(".what-for__slider").owlCarousel({
			loop: true,
			autoplay: false,
			items: 1,
			nav: true,
			autoplayTimeout: 5000,
			dots: false,
			margin: 10,
		});
		$('.owl-prev, .owl-next').text('');
		setTimeout(function(){
			$('.what-for__slider').addClass('active');
		},1500);

		$('.activities__close').on('click', function(){
			$('.activities__info').removeClass('active');
			content_scrollTop();
		});

		$('body').on('click', function(event){
			if (!$(event.target).parents('').is('.what-for__item') && !$(event.target).parents('').is('.activities__info') && !$(event.target).is('.what-for__item')) {
				$('.activities__info').removeClass('active');
				content_scrollTop();
			}
		});

		$('.what-for__item').on('click', function(event) {
			$('.activities__info').removeClass('active');
			event.preventDefault();
			let activities = $(this).attr('data-activities');
			$('.activities__info[data-activities='+activities+']').addClass('active');
		});
		contentScroll();

		let locat = location.href;
		let arr_location = locat.split('#');
		setTimeout(function(){
			$('.activities__info[data-activities='+arr_location[1]+']').addClass('active');
		},2000);
	}


	sect_how_many();
	function sect_how_many() {
		$(".business__slider").owlCarousel({
			loop: true,
			autoplay: false,
			items: 1,
			nav: true,
			autoplayTimeout: 5000,
			dots: false,
			margin: 20,
		});
		$('.owl-prev, .owl-next').text('');
		setTimeout(function(){
			$('.business__slider').addClass('active');
		},1500);

		$('.business__close').on('click', function(){
			$('.business__info').removeClass('active');
			content_scrollTop();
		});

		$('body').on('click', function(event){
			if (!$(event.target).parents('').is('.business-slider__item') && !$(event.target).parents('').is('.business__info') && !$(event.target).is('.business-slider__item')) {
				$('.business__info').removeClass('active');
				content_scrollTop();
			}
		});

		$('.business-slider__item').on('click', function(event) {
			$('.business__info').removeClass('active');
			event.preventDefault();
			let activities = $(this).attr('data-activities');
			$('.business__info[data-activities='+activities+']').addClass('active');
		});
		contentScroll();
	}

	sect_what();
	function sect_what() {
		$('.what__video').on('click', function(){
			let audio = document.getElementById("audio-player");
			if ($(this).is('.active')) {
				$(this).removeClass('active');
				$(this).find('video').get(0).pause();
				audio.play();
			} else {
				$(this).find('video').get(0).play();
				$(this).addClass('active');
				audio.pause();
				$('.mCSB_container').animate( { top: '-230px' }, 500 );
				$('.content__scroll .mCSB_dragger').css({
					top: "57px"
				});
			}
		});

		$('.video__icon-play').on('click', function(){
			$(this).addClass('video-play');
			$('.video-item.active').find('video').get(0).play();
		});
		contentScroll();

			}


	function content_scrollTop() {
		$('.content__scroll .mCSB_container').css({
			top: "0px"
		});
		$('.content__scroll .mCSB_dragger').css({
			top: "0px"
		});
	}

	sect_support();
	function sect_support() {
		contentScroll();
		$( "#support-select" ).change(function() {
			let option_select = $("select option:selected").attr('value');
			if ($("select option:selected").attr('value')== "filter-all" == true) {
				$('.support__content-item').addClass('active');
			} else {
				$('.support__content-item').removeClass('active');
				$('.support__content-item[data-filter='+option_select+']').addClass('active');
			}
		});

	}

	$(window).resize(function(){
		contentScroll();
	});

	contentScroll();
	function contentScroll() {
		if ( $('section').is('.sect__what') && $(window).height() < 900 || $('section').is('.sect__support') && $(window).height() < 900 ) {
			$('.content__scroll').mCustomScrollbar("destroy");
		} else {
			var $cardWrapper = $('.content__scroll');
			if ($cardWrapper.length > 0) {
			    $cardWrapper.mCustomScrollbar({
			        scrollInertia: 50,
			        scrollbarPosition: 'outside',
			        scrollButtons: {
			            enable: true
			        }
			    });
			}
		}
	}

	sect_history();
	function sect_history() {
		$(".history__slider").owlCarousel({
			loop: true,
			autoplay: false,
			items: 1,
			nav: true,
			autoplayTimeout: 5000,
			dots: false,
			margin: 100,
		});
		$('.owl-prev, .owl-next').text('');
		setTimeout(function(){
			$('.history__slider').addClass('active');
		},1500);

		$('.history__slider-item').on('click', function(){
			let history_item = $(this).attr('data-history');
			$('.history__info').removeClass('active');
			$('.history__info[data-history='+history_item+']').addClass('active');
		});
		$('.history__close').on('click', function(){
			$('.history__slider-item').removeClass('active');
			$('.history__info').removeClass('active');
			content_scrollTop();
		});

		$('body').on('click', function(event){
			if (!$(event.target).parents('').is('.history__slider-item') && !$(event.target).parents('').is('.history__info') && !$(event.target).is('.history__slider-item')) {
				$('.history__slider-item').removeClass('active');
				$('.history__info').removeClass('active');
				content_scrollTop();
			}
		});
		contentScroll();

		let locat = location.href;
		let arr_location = locat.split('#');
		setTimeout(function(){
			$('.history__info[data-history='+arr_location[1]+']').addClass("active");
		},2000)

	}


	function certificate_view() {
		$('.certificate-wrapper').addClass('active');
		$('.certificate_name-block .surname').text($('.feedback-surname').val());
		$('.certificate_name-block .name').text($('.feedback-name').val());
		$('.certificate_name-block .patronymic').text($('.feedback-patronymic').val());

		let select_region = $('#select_region').val();
		let select_activitie = $('#select_activities').val();
		if (select_region == null) {
			$('.choice-region').hide();
		} else {
			$('.choice-region').show();
			$('.choice-region span').text($('#select_region option[value='+select_region+']').text());
		}
		if (select_activitie == null) {
			$('.choice-activities').hide();
		} else {
			$('.choice-activities').show();
			$('.choice-activities span').text($('#select_activities option[value='+select_activitie+']').text());
		}
	}

	function certificate_business_view() {
		$('.certificate-wrapper').addClass('active');
		$('.certificate_name-block .surname').text($('.feedback-surname').val());
		$('.certificate_name-block .name').text($('.feedback-name').val());
		$('.certificate_name-block .patronymic').text($('.feedback-patronymic').val());
	}

	sect_feedback();
	function sect_feedback() { 
		jsKeyboard.init("virtualKeyboard");
		$("#keyboardSymbols,#keyboardCapitalLetter,#keyboardNumber,#keyboardSmallLetter,#keyboardRuSmallLetter").css("display", "none");
		$("#keyboardRuCapitalLetter").css("display", "block");
		var $firstInput = $(':input').first().focus();
		jsKeyboard.currentElement = $firstInput;
		jsKeyboard.currentElementCursorPosition = 0;

		$('input[type=email]').on('click', function(){
			$("#keyboardRuCapitalLetter,#keyboardSymbols,#keyboardNumber,#keyboardSmallLetter,#keyboardRuSmallLetter").css("display", "none");
			$('#keyboardCapitalLetter').css("display", "block");
		});

		$('.input__wrapper-first input').on('click', function(){
			$("#keyboardSymbols,#keyboardCapitalLetter,#keyboardNumber,#keyboardSmallLetter,#keyboardRuSmallLetter").css("display", "none");
			$("#keyboardRuCapitalLetter").css("display", "block");
		});

		let locat = location.href;
		let arr_location = locat.split('#');
		if (arr_location[1] == 'bili' || arr_location[1] == 'egve' || +
			arr_location[1] == 'prov' || arr_location[1] == 'anad' || +
			arr_location[1] == 'chu' || arr_location[1] == 'pevek' 
			) {
			$('#select_region option[value='+arr_location[1]+']').attr("selected", "selected");
		}


		$('.sect__feedback .form-submit').on('click', function(){
			if ($(this).parents('form').is('.sending-form')) {
			    return false;
			}
			var inputs = $(this).parents('form').find('.required');
			for (let i = 0; i < inputs.length; i++) {
				if ($.trim(inputs.eq(i).val()) === '') {
					$(this).parents('form').find('.required').eq(i).addClass('input-error');
				}
			}
			if ($('.feedback-form .required.input-error').length > 0) {
				 return false;
			}
			if ($(this).parents('form').find('input').is('.input-error')){
				return false;
			}
			var valid = $(this).parents('form').find('input[type="email"]').val();
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if(!valid.match(mailformat)){
				$(this).parents('form').find('input[type="email"]').addClass('input-error');
				return false;
			}

			var form_submit = $(this).parents('form');
			$(this).parents('form').addClass('sending-form');
			sendFormCall(form_submit);


			$('.certificate__close').on('click', function(){
				clearTimeout(index_redirect);
			});


			var index_redirect = setTimeout(function(){
				var toLoad = 'index.html #page-wrapper';
				var toSRC = 'index.html';
				$('#page-wrapper').fadeIn('fast',loadContent());
				history.pushState(null, null, toSRC);
				function loadContent() {
					$('#page-wrapper').load(toLoad,'',showNewContent);
					$('.menu_wrapper').removeClass('active');
					$('#page-wrapper').removeClass('background-none');
					$('.preload-wrapper').addClass('animate');
				}
				function showNewContent() {
					$(toLoad).children().unwrap();
					setTimeout(function(){
						$('.preload-wrapper').removeClass('animate');
					},1000)
					$('.content').hide();
					if ($('.menu_wrapper').is('.active')) {
						$('.menu_wrapper').removeClass('active');
						setTimeout(function(){
							$('.content').fadeOut('800');
						},800)
						setTimeout(function(){
							$('.content').fadeIn('800');
						},1600);
					} else {
						$('.content').fadeOut('800');
						setTimeout(function(){
							$('.content').fadeIn('800');
						},1200);
					}
				}
				return false;
			},60000)

					});

		$('input').on('click', function(){
			$(this).removeClass('input-error');
		});


     function sendFormCall(form_submit) {
      $.ajax({
        url: $(form_submit).attr('action'),
        type: $(form_submit).attr('method'),
        dataType: 'json',
        data: $(form_submit).serialize(),
        success: function(data) {
            certificate_view();
            if (data.error !== undefined) {
                console.error(data.error);
                return false;
            } else if (data.success !== undefined) {

            }   
        },
        error: function() {
          console.error('Ошибка запроса к серверу');
        }
      });
    }


	}


	sect_how_many_feedback();
	function sect_how_many_feedback() { 
		jsKeyboard.init("virtualKeyboard");
		$("#keyboardSymbols,#keyboardCapitalLetter,#keyboardNumber,#keyboardSmallLetter,#keyboardRuSmallLetter").css("display", "none");
		$("#keyboardRuCapitalLetter").css("display", "block");
		var $firstInput = $(':input').first().focus();
		jsKeyboard.currentElement = $firstInput;
		jsKeyboard.currentElementCursorPosition = 0;

		$('input[type=email]').on('click', function(){
			$("#keyboardRuCapitalLetter,#keyboardSymbols,#keyboardNumber,#keyboardSmallLetter,#keyboardRuSmallLetter").css("display", "none");
			$('#keyboardCapitalLetter').css("display", "block");
		});

		$('.input__wrapper-first input').on('click', function(){
			$("#keyboardSymbols,#keyboardCapitalLetter,#keyboardNumber,#keyboardSmallLetter,#keyboardRuSmallLetter").css("display", "none");
			$("#keyboardRuCapitalLetter").css("display", "block");
		});

		let locat = location.href;
		let arr_location = locat.split('#');
		$('#select-business-plan option[value='+arr_location[1]+']').attr("selected", "selected");


				$('.sect__how-many-feedback .form-submit').on('click', function(){
			if ($(this).parents('form').is('.sending-form')) {
				return false;
			}
			var inputs = $(this).parents('form').find('.required');
			for (let i = 0; i < inputs.length; i++) {
				if ($.trim(inputs.eq(i).val()) === '') {
					$(this).parents('form').find('.required').eq(i).addClass('input-error');
				}
			}
			if ($('.feedback-form .required.input-error').length > 0) {
				 return false;
			}
			if ($(this).parents('form').find('input').is('.input-error')){
				return false;
			}
			var valid = $(this).parents('form').find('input[type="email"]').val();
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if(!valid.match(mailformat)){
				$(this).parents('form').find('input[type="email"]').addClass('input-error');
				return false;
			}

			var form_submit = $(this).parents('form');
			$(this).parents('form').addClass('sending-form');
			sendFormCallBusiness(form_submit);

			$('.certificate__close').on('click', function(){
				clearTimeout(index_redirect);
			});


			var index_redirect = setTimeout(function(){
				var toLoad = 'index.html #page-wrapper';
				var toSRC = 'index.html';
				$('#page-wrapper').fadeIn('fast',loadContent());
				history.pushState(null, null, toSRC);
				function loadContent() {
					$('#page-wrapper').load(toLoad,'',showNewContent);
					$('.menu_wrapper').removeClass('active');
					$('#page-wrapper').removeClass('background-none');
					$('.preload-wrapper').addClass('animate');
				}
				function showNewContent() {
					$(toLoad).children().unwrap();
					setTimeout(function(){
						$('.preload-wrapper').removeClass('animate');
					},1000)
					$('.content').hide();
					if ($('.menu_wrapper').is('.active')) {
						$('.menu_wrapper').removeClass('active');
						setTimeout(function(){
							$('.content').fadeOut('800');
						},800)
						setTimeout(function(){
							$('.content').fadeIn('800');
						},1600);
					} else {
						$('.content').fadeOut('800');
						setTimeout(function(){
							$('.content').fadeIn('800');
						},1200);
					}
				}
				return false;
			},60000)

		});

		$('input').on('click', function(){
			$(this).removeClass('input-error');
		});

		 function sendFormCallBusiness(form_submit) {
	      $.ajax({
	        url: $(form_submit).attr('action'),
	        type: $(form_submit).attr('method'),
	        dataType: 'json',
	        data: $(form_submit).serialize(),
	        success: function(data) {
	            certificate_business_view()
	            if (data.error !== undefined) {
	                console.error(data.error);
	                return false;
	            } else if (data.success !== undefined) {

	            }   
	        },
	        error: function() {
	          console.error('Ошибка запроса к серверу');
	        }
	      });
	    }

	}






	$('.btn_deer-animate').on('click', function() {
		if ($('.deer').is('.step-1')) {
			$('.deer').removeClass('step-1');
			$('.deer').addClass('step-2');
			return false;
		}
		if ($('.deer').is('.step-2')) {
			$('.deer').removeClass('step-2');
			$('.deer').addClass('step-1');
			return false;
		}
	});


	audio_main();
	function audio_main() {
		let audio = document.getElementById("audio-player");
	}




	});

